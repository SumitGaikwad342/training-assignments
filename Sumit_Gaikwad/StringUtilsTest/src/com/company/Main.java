package com.company;
import java.util.Scanner;
import org.apache.commons.lang3.*;



public class Main {

    public static Scanner scanner=new Scanner(System.in);
    public static void main(String[] args) {

        System.out.println("****MENU****");
        System.out.println("1.StringUtils.equals()" +
                "\n2.StringUtils.split() and StringUtils.join() " +
                "\n3.StringUtils.isEmpty() and StringUtils.isBlank() " +
                "\n4.StringUtils.compare() " +
                "\n5.StringUtils.indexOf() " +
                "\n6.StringUtils.lastIndexOf() " +
                "\n7.StringUtils.contains() " +
                "\n8.StringUtils.substring() " +
                "\n9.StringUtils.remove() " +
                "\n10.StringUtils.replace()" +
                "\n11.StringUtils.countMatches()" +
                "\n0.Exit  ");
        System.out.println("Enter your choice");
        int choice = scanner.nextInt();
        scanner.nextLine();
        switch (choice) {
            case 0:
                break;
            case 1:
                stringEqual();
                break;
            case 2:
                stringSplit();
                break;
            case 3:
                stringEmpty();
                break;
            case 4:
                stringCompare();
                break;
            case 5:
                stringIndexOf();
                break;
            case 6:
                stringLastIndexOf();
                break;
            case 7:
                stringContains();
                break;
            case 8:
                subString();
                break;
            case 9:
                stringRemove();
                break;
            case 10:
                stringReplace();
                break;
            case 11:
                countMatches();
                break;

        }

    }


    public static void stringEqual() {
        //StringUtils.equals()
        System.out.println("Enter 1st String: ");
        String str_a = scanner.nextLine();
        System.out.println("Enter 2nd String: ");
        String str_b = scanner.nextLine();
        if (StringUtils.equals(str_a, str_b)) {
            System.out.println("Strings are Equal");
        } else {
            System.out.println("Strings are not equal");
        }
    }

    public static void stringSplit(){
        System.out.println("Enter your string");
        String str= scanner.nextLine();
        String[] str_array=StringUtils.split(str," ");
        for(String s:str_array){
            System.out.println(s);
        }
        System.out.println("Your String is splitted successfully!");

        System.out.println(StringUtils.join(str_array,";"));
        System.out.println("Your string is joined again!");
    }

    public static void stringEmpty(){
        String nullString = null;
        String emptyString = "";
        String blankString = " \n \t ";

        if(nullString != null && !nullString.isEmpty()) {
            System.out.println("nullString isn't null");
        }

        if(StringUtils.isEmpty(emptyString)) {
            System.out.println("emptyString is empty");
        }

        if(StringUtils.isBlank(blankString)) {
            System.out.println("blankString is blank");
        }
    }

    public static void stringCompare(){
        System.out.println("Enter 1st string: ");
        String str=scanner.nextLine();
        System.out.println("Enter 2nd string you want to compare with 1st: ");
        String  str_c= scanner.nextLine();
        int result= StringUtils.compare(str,str_c);
        if(result==0){
            System.out.println("Strings are equal");
        }else if(result<0){
            System.out.println("1st string is less than 2nd string");
        }else{
            System.out.println("1st string is greater than 2nd string");
        }
    }

    public static void stringIndexOf(){
        System.out.println("Enter String: ");
        String str= scanner.nextLine();
        System.out.println("Enter the search sequence you: ");
        String search_seq= scanner.nextLine();
        int index=StringUtils.indexOf(str,search_seq);
        if(index== -1){
            System.out.println("Either no match or input string is null");
        }else{
            System.out.println("Search sequence is found in a given string at " + index + "th position");
        }
    }

    public static void stringLastIndexOf(){
        System.out.println("Enter String: ");
        String str= scanner.nextLine();
        System.out.println("Enter the search sequence you: ");
        String search_seq= scanner.nextLine();
        int index=StringUtils.lastIndexOf(str,search_seq);
        if(index== -1){
            System.out.println("Either no match or input string is null");
        }else{
            System.out.println("Last occurence of Search sequence is found in a given string at " + index + "th position");
        }
    }

    public static void stringContains(){
        System.out.println("Enter a string: ");
        String str= scanner.nextLine();
        System.out.println("Enter a sub-string you want to search");
        String substr= scanner.nextLine();
        boolean result=StringUtils.contains(str,substr);
        if(result){
            System.out.println("Substring " + substr + " found in a given string at " + str.indexOf(substr) + " this position");
        }else{
            System.out.println("Substring not found in a given string" );
        }
    }

    public static void subString(){
        System.out.println("Enter a String: ");
        String str= scanner.nextLine();
        System.out.println("Enter starting position: ");
        int start= scanner.nextInt();
        System.out.println(StringUtils.substring(str,start)); //we can also pass ending position as a parameter
    }

    public static void stringRemove(){
        System.out.println("Enter a string: ");
        String str= scanner.nextLine();
        System.out.println("Enter a character/substring that you want to remove:");
        String substr= scanner.nextLine();
        System.out.println(StringUtils.remove(str,substr));
    }

    public static void stringReplace(){
        System.out.println("Enter a string");
        String str= scanner.nextLine();
        System.out.println("Enter a sub string you want to replace:");
        String oldstr= scanner.nextLine();
        System.out.println("Enter a new string you want to replace with old string:");
        String newstr= scanner.nextLine();
        System.out.println(StringUtils.replace(str,oldstr,newstr));
    }

    public static void countMatches(){
        System.out.println("Enter a string");
        String str= scanner.nextLine();
        System.out.println("Enter a character you want to count:");
        String substr= scanner.nextLine();
        int count= StringUtils.countMatches(str,substr);
        System.out.println("Substring " + substr + " occuring " + count + " times");

    }
}
